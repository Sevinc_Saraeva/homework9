package Services;

import Controllers.FamilyController;
import DAO.CollectionFamilyDao;
import Entitiy.Family;
import Entitiy.Human;
import Entitiy.Pet;
import Entitiy.Species;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    Family family;
    FamilyService familyService;
    CollectionFamilyDao collectionFamilyDao;
    @BeforeEach
    void init(){
        FamilyController familyController = new FamilyController();
        familyService = new FamilyService();
         family = new Family(new Human("Lili", "AAA", 1958),(new Human("Ali", "AAA", 1958)));

    }

    @Test
    void getAllFamilies() {
       assertEquals(collectionFamilyDao.getAllFamilies(), familyService.getAllFamilies());

    }

//    @Test
//    void displayAllFamilies() {
//    }
//
//    @Test
//    void getFamiliesBiggerThan() {
//
//    }
//
//    @Test
//    void getFamiliesLessThan() {
//    }
//
//    @Test
//    void countFamiliesWithMemberNumber() {
//    }
//
//    @Test
//    void createNewFamily() {
//    }
//
//    @Test
//    void deleteFamilyByIndex() {
//    }
//
//    @Test
//    void adoptChild() {
//    }
//
//    @Test
//    void deleteAllChildrenOlderThen() {
//    }
//
//    @Test
//    void count() {
//    }
//
//    @Test
//    void getFamilyById() {
//    }
//
//    @Test
//    void getPets() {
//    }
//
//    @Test
//    void addPet() {
//    }
//
//    @Test
//    void bornChild() {
//    }
//
//    @Test
//    void addFamily() {
//    }
}
import Controllers.FamilyController;
import Entitiy.*;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();
        Family family = new Family(new Human("Lili", "AAA", 1958),(new Human("Ali", "AAA", 1958)));
        familyController.saveFamily(family);
        familyController.bornChild(family, "", "Orkhan");
        familyController.adoptChild(family, new Human("AA", "BBB", 2000));
        familyController.addPet(0, new Fish( "fish"));
        List<Family> allFamilies = familyController.getAllFamilies();
        familyController.displayAllFamilies();
        System.out.println(familyController.count());

    }
}

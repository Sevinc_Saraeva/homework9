package Services;

import DAO.CollectionFamilyDao;
import Entitiy.*;
import java.time.Year;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    CollectionFamilyDao collectionFamilyDao;

    public FamilyService() {
        collectionFamilyDao = new CollectionFamilyDao();
 }

 public List<Family> getAllFamilies(){
        return  collectionFamilyDao.getAllFamilies();
 }

 public void displayAllFamilies(){
        List<Family> families = getAllFamilies();
     for (Family family: families) {
         System.out.println(family.toString());
     }
 }
 public void getFamiliesBiggerThan(int number){
     getAllFamilies().stream().filter(family -> family.countFamily()>number).peek(x-> System.out.println(x.toString()));

 }
    public void getFamiliesLessThan(int number){
        getAllFamilies().stream().filter(family -> family.countFamily()<number).peek(x-> System.out.println(x.toString()));

    }
    public int countFamiliesWithMemberNumber(int number){
        return  getAllFamilies().stream().filter(family -> family.countFamily()==number).collect(Collectors.toList()).size();
    }

public  boolean createNewFamily(Human mother, Human father){
        return  collectionFamilyDao.saveFamily(new Family(mother, father));

}
public boolean deleteFamilyByIndex(int index){
        return collectionFamilyDao.deleteFamily(index);
}
public Family adoptChild(Family family, Human child){
    for (int i = 0; i < getAllFamilies().size(); i++) {
        if(getAllFamilies().get(i).equals(family)){
            getAllFamilies().get(i).addChild(child);
        }
        return family;
    }
    throw  new RuntimeException("No family found");
}

public void deleteAllChildrenOlderThen(int number) {
    for (Family family: getAllFamilies()) {
       List<Human> collected=  family.getChildren().stream().filter(child-> Year.now().getValue()-child.getYear()==number).collect(Collectors.toList());
          collected.forEach(family::deleteChild);
    }
}

public int count(){
        return  getAllFamilies().size();
}
public  Family getFamilyById(int index){
        return getAllFamilies().get(index);
}
    public List<Pet> getPets(int index){
        return  getAllFamilies().get(index).getPet();
    }

    public void addPet(int index, Pet pet){
        Family fam =  getAllFamilies().get(index);
        fam.getPet().add(pet);
        getAllFamilies().set(index,fam);
    }



public Family bornChild(Family family, String feminine, String masculine){
    for (int i = 0; i < getAllFamilies().size(); i++) {
        if (getAllFamilies().get(i).equals(family)) {
            if (feminine.equals("")) {
                getAllFamilies().get(i).addChild(new Human(masculine, family.getFather().getSurname(), 2020));
            } else if (masculine.equals("")) {
                getAllFamilies().get(i).addChild(new Human(feminine, family.getFather().getSurname(), 2020));
            } else {
                getAllFamilies().get(i).addChild(new Human(masculine, family.getFather().getSurname(), 2020));
                getAllFamilies().get(i).addChild(new Human(feminine, family.getFather().getSurname(), 2020));
            }
        }
    }
        return  family;

}
public  boolean addFamily(Family family){
        return collectionFamilyDao.saveFamily(family);
}






}